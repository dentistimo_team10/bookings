// to test the breaker, simply run this file `node server/breaker/testBreaker.js  `

const { v4: uuidv4 } = require('uuid');
const CircuitBreaker = require("opossum");
const circuitSettings = require("./breaker");
const appointmentController = require("../controllers/controllers.appointments");
const db = require("../DB/db");
db.connect;

// use a breaker to test creation of a booking
const breaker = new CircuitBreaker(
  appointmentController.createAppointment,
  circuitSettings.options
);
circuitSettings.addSettings(breaker);

// will pass because it follows schema
const successfulBooking = {
  dentistID: 1,
  userId: "1234567891",
  issuance: uuidv4(),
  requestId: 1,
  selectedTime: ["14:30"],
  selectedDate: ["2022-01-11"]
};

// will fail because of required fields not added
const unsuccessfulBooking = {
  "userId": "3323",
};
function pass() {
  breaker.fire(successfulBooking).then(console.log).catch(console.error);
}

function fail() {
  breaker.fire(unsuccessfulBooking).then(console.log).catch(console.error);
}

function waitAndExecute(func, milliseconds){
  setTimeout(func, milliseconds)
}

// cause breaker to open by exceeding threshold of failures

let timesToFail = 20;

for(let i = 0; i < timesToFail; i++){
  fail()
}

// wait until breaker is in half open state and try a successful event, will close the breaker
waitAndExecute(pass, 3000)