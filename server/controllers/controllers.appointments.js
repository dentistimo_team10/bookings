// db transactions should follow this format to use circuitbreaker (returning a promise and rejecting or resolving it)
const Appointment = require("../Models/Appointments");

const createAppointment = (body) => {
  return new Promise((resolve, reject) => {
    // Generate a random UUID for issuance
    const a = new Appointment(body)
    a.save().then(data => resolve({ msg: data })).catch(err => reject({ err: err }));
  })
};

module.exports = { createAppointment }