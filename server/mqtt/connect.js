const connect = {
    host: 'broker.emqx.io',
    port: 8083,
    endpoint: '/mqtt',
    clean: true,
    connectTimeout: 4000, 
    reconnectPeriod: 4000, 
    clientId: 'team-10-dentistimo-bookingmanager',
    username: 'team_10',
    password: 'team_10'
  }

const connectUrl = `ws://${connect.host}:${connect.port}${connect.endpoint}`
 // export connection details
 module.exports = {connect, connectUrl}
