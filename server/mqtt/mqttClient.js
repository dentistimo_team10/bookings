/**
 * This is for initializing MQTT
 * Author(s): Hannah, Lucas, Anna
 */

 const CircuitBreaker = require("opossum");
 const circuitSettings = require("../breaker/breaker");
 const appointmentController = require("../controllers/controllers.appointments");
 const requestValidator = require('../utilities/requestValidator')

 const Publisher = require("./publisher");
 const Subscriber = require("./subscriber");
 const topics = require("./topics");
 const subscribeTopics = topics.SUBSCRIBE_TOPICS;
 const publishTopics = topics.PUBLISH_TOPICS;
 
 const mqtt = require("mqtt");
 const connection = require("./connect");
 const publish = new Publisher();
 const subscribe = new Subscriber();
 
 const client = mqtt.connect(connection.connectUrl, connection.connect);
 
 /**
  * Class used to handle anything mqtt related
  */
 class MqttClient {
   constructor() {
     // Connect
     client.on("connect", function () {
       console.log("Connected");
     });
 
     // create a new breaker for the creation of booking
     const breaker = new CircuitBreaker(
       appointmentController.createAppointment,
       circuitSettings.options
     );
     circuitSettings.addSettings(breaker);
 
     // subscribe to booking request from clientui
     subscribe.subscribeToTopic(subscribeTopics.BOOKING_REQUEST, client)
 
    // Receive messages and do stuff
    client.on("message", function (topic, message) {
      console.log(message.toString());
      let subscription = new RegExp("^(?:" + subscribeTopics.BOOKING_REQUEST.replace('#', '') + ")(.+)$")
      if (topic.match(subscription) !== null) {
        const userId = topic.match(subscription)[1];
        // First matching group equals to the client ID
        let validation = requestValidator.validateRequest(message)
        // If received request passes validation
        if (validation !== null && validation.valid) {
          breaker.fire(JSON.parse(message)).then((res) => {
            publish.publishData(publishTopics.BOOKING_RESPONSE + userId, JSON.stringify(res), client);
          }).catch((err) => {
            publish.publishData(publishTopics.BOOKING_RESPONSE + userId, JSON.stringify(err), client);
          });
        } else {
          // Define invalid request object
          const invalidReq = {
            err: "Invalid appointment request!",
            timestamp: new Date().toISOString(),
            request: message
          }

          // If message is not a valid JSON object
          if (validation === null) {
            invalidReq.reason = "Message is not a valid JSON!"
          } else {
            invalidReq.reason = JSON.stringify(validation.errors)
          }
          publish.publishData(publishTopics.BOOKING_RESPONSE + userId, JSON.stringify(invalidReq), client);
        }
      }
    });
 
     client.on("close", () => {
       console.log(`mqtt client disconnected`);
     });
 
     client.on("error", (error) => {
       console.log("Connection failed", error);
     });
   }
 }
 
 module.exports = MqttClient;
