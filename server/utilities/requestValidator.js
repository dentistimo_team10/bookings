var Validator = require('jsonschema').Validator;

// Main request message body
var requestSchema = {
    id: "/SimpleRequest",
    type: "object",
    properties: {
      "dentistID": { type: "integer", required: true },
      "userId": { type: "string", required: true },
      "issuance": { type: "string", required: true },
      "requestId": { type: "integer", required: true },
      "selectedTime": { type: "array", items: { type: "string", required: true }, required: true},
      "selectedDate": { type: "array", items: { type: "string", required: true }, required: true}
    }
}

// Validate if message string follows required format
const validateRequest = function(req) {
    if (validateJSON(req) === true) {
        var v = new Validator();
        req = JSON.parse(req)
        return v.validate(req, requestSchema, {required: true})
    }
    return null
}

// Validate if message string is in a valid JSON format
const validateJSON = function(req) {
    try {
        json = JSON.parse(req)
        return true
    } catch(e) {
        return false
    }
}

module.exports = { validateRequest }